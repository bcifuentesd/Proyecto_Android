package curso.umg.gt.miaplicacion;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class TeacherActivity extends AppCompatActivity {
    private List<Nota> Notas= new ArrayList<>();
    private ListView lista;
    private EditText et1,et2, et3;
      private ArrayAdapter<Nota> adapter;
    private Button bt2;
    private ArrayList<String> listado;
    AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this,
            "administracion", null, 1);



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher);
        lista= (ListView) findViewById(R.id.lv1);
        et1= (EditText) findViewById(R.id.etT1);
        et2= (EditText) findViewById(R.id.etT2);
        et3= (EditText) findViewById(R.id.etT3);

        adapter = new ArrayAdapter<Nota>(this, android.R.layout.simple_list_item_1,Notas);
        lista.setAdapter(adapter);
        showAll();






    }
    // Establecemos la funcionalidad del botón
   public void add (View view){
       SQLiteDatabase bd = admin.getWritableDatabase();
       int car = Integer.parseInt(et1.getText().toString());
       String nombre = et2.getText().toString();
       int nota = Integer.parseInt(et1.getText().toString());

       ContentValues registro = new ContentValues();
       registro.put("carne", car);
       registro.put("nombre", nombre);
       registro.put("nota",nota);

       bd.insert("Nota", null, registro);
       bd.close();

       et1.setText("");
       et2.setText("");
       et3.setText("");
       Toast.makeText(this, "Alumno Agregado",
               Toast.LENGTH_SHORT).show();

       showAll();
       }

    public void showAll() {
        String selectAllQuery = "SELECT * FROM Nota";
        SQLiteDatabase bd = admin.getReadableDatabase();
        Notas.clear();

        Cursor c = bd.rawQuery(selectAllQuery, null);
       try {
            while (c.moveToNext()) {
                Nota a = new Nota();
                a.setCarne(c.getInt(c.getColumnIndex("carne")));
                a.setNombre(c.getString(c.getColumnIndex("nombre")));
                a.setNota(c.getInt(c.getColumnIndex("nota")));

                Notas.add(a);
            }
        } finally {
            c.close();
        }
        adapter.notifyDataSetChanged();
    }

}
