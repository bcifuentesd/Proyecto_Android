package curso.umg.gt.miaplicacion;

/**
 * Created by laptop on 26/07/2017.
 */

public class Encuesta {
    public int getCarne() {
        return carne;
    }

    public void setCarne(int carne) {
        this.carne = carne;
    }

    int carne;
    String nombreAlumno;
    String email;
    int telefono;
    int grado;
    int genero;
    public Encuesta(){

    }
    public Encuesta(int carne,String nombreAlumno, String email, int telefono, int grado, int genero) {
        this.carne=carne;
        this.nombreAlumno = nombreAlumno;
        this.email = email;
        this.telefono = telefono;
        this.grado = grado;
        this.genero = genero;
    }
    public String getNombreAlumno() {
        return nombreAlumno;
    }

    public void setNombreAlumno(String nombreAlumno) {
        this.nombreAlumno = nombreAlumno;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public int getGrado() {
        return grado;
    }

    public void setGrado(int grado) {
        this.grado = grado;
    }

    public int getGenero() {
        return genero;
    }

    public void setGenero(int genero) {
        this.genero = genero;
    }









}
