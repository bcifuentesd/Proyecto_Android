package curso.umg.gt.miaplicacion;

/**
 * Created by laptop on 25/07/2017.
 */

public class Nota {
    private int carne;
    private String nombre;
    private int nota;



    public Nota(int carne, String nombre, int nota) {
        this.carne = carne;
        this.nombre = nombre;
        this.nota = nota;
    }

    public Nota(){

    }


    public int getCarne() {
        return carne;
    }

    public void setCarne(int carne) {
        this.carne = carne;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }
}
