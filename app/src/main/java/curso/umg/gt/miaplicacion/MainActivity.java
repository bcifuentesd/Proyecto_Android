package curso.umg.gt.miaplicacion;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    private EditText et1, et2;
    private Button bt1;
    private View view;
    private TextView tv1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et1 = (EditText)findViewById(R.id.et1);
        et2 =(EditText)findViewById(R.id.et2);
        bt1= (Button) findViewById(R.id.bt1);
        tv1=(TextView) findViewById(R.id.tv1);

    }
    public void logueo (View view){
        String usuario = et1.getText().toString();
        String pass = et2.getText().toString();
        //tv1.setText("ingreso a logueo");

        if (usuario.endsWith("@profesor.gt")){
            Intent i= new Intent(MainActivity.this, TeacherActivity.class);
            startActivity(i);

        }
        else {

            Toast notification = Toast.makeText(this, "Credenciales Invalidas", Toast.LENGTH_SHORT);
        }
        if(usuario.endsWith("alumno.gt"))
        {
            Intent i=new Intent(MainActivity.this,AlumnoActivity.class);
            startActivity(i);
        }

    }
}
