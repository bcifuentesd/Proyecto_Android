package curso.umg.gt.miaplicacion;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;


public class AlumnoActivity extends AppCompatActivity {
    private List<Encuesta> Encuestas= new ArrayList<>();
    private ListView encuesta;
    private EditText et1,et2, et3,et4;
    private RadioButton rb1, rb2, rb3,rb4;
    private ArrayAdapter<Encuesta> adapter;
    private Button enviar;
    private ArrayList<String> listado;


    AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this,
            "administracion", null, 1);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alumno);

        encuesta= (ListView) findViewById(R.id.lv1);
        et1= (EditText) findViewById(R.id.etcarne);
        et2= (EditText) findViewById(R.id.et4);
        et3= (EditText) findViewById(R.id.et5);
        et4= (EditText) findViewById(R.id.et6);
        adapter = new ArrayAdapter<Encuesta>(this,android.R.layout.simple_list_item_1,Encuestas );
        encuesta.setAdapter(adapter);
        showAll();
    }

    public void enviar (View view){
        SQLiteDatabase bd = admin.getWritableDatabase();
        int car = Integer.parseInt(et1.getText().toString());
        String nombre = et2.getText().toString();
        String email=et3.getText().toString();
        int telefono = Integer.parseInt(et4.getText().toString());

        ContentValues registro = new ContentValues();
        registro.put("carne", car);
        registro.put("nombre", nombre);
        registro.put("email",email);
        registro.put("telefono",telefono);

        bd.insert("Encuesta", null, registro);
        bd.close();

        et1.setText("");
        et2.setText("");
        et3.setText("");
        et4.setText("");
        Toast.makeText(this, "Encuesta enviada",
                Toast.LENGTH_SHORT).show();

        showAll();
    }

    private void showAll() {

        String selectAllQuery = "SELECT * FROM Nota";
        SQLiteDatabase bd = admin.getReadableDatabase();
        Encuestas.clear();

        Cursor c = bd.rawQuery(selectAllQuery, null);
        try {
            while (c.moveToNext()) {
                Encuesta a = new Encuesta();
                a.setCarne(c.getInt(c.getColumnIndex("carne")));
                a.setNombreAlumno(c.getString(c.getColumnIndex("nombre")));
                a.setEmail(c.getString(c.getColumnIndex("email")));
                a.setTelefono(c.getInt(c.getColumnIndex("telefono")));
                a.setGenero(1);
                a.setGrado(1);




                Encuestas.add(a);
            }
        } finally {
            c.close();
        }
        adapter.notifyDataSetChanged();
    }
}
